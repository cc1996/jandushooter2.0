﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cannon : MonoBehaviour {

	public float couldown;
	private float timeCounter;
	private bool canShoot;

	void Start(){
		canShoot = true;
		timeCounter = 0;
	}

	void Update(){
		if (!canShoot) {
			timeCounter += Time.deltaTime;
			if (timeCounter >= couldown) {
				canShoot = true;
			}
		}
	}

	public void ShootCannon(Cartridge cart){
		if (canShoot) {
			canShoot = false;
			timeCounter = 0;

			cart.GetBullet ().Shot (transform.position, 0f);
		}
	}
}