﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapons : MonoBehaviour {

	public Cannon cannon;
	//Create bullets
	public int maxBullets;
	public Transform bulletPosition;
	private Cartridge[] cart;

	// Use this for initialization
	void Start () {
		GameObject[] bullets = Resources.LoadAll<GameObject> ("Prefabs/Bullets");

		cart = new Cartridge[bullets.Length];

		Vector2 spawnpos = bulletPosition.position;

		for (int i = 0; i < bullets.Length; i++) {
			cart[i] = new Cartridge(bullets[i], bulletPosition, spawnpos, maxBullets);
			spawnpos.y += 1;
		}

	}

	public void Shoot(){
		cannon.ShootCannon (cart[0]);
	}
}